package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	addr := ":8080"

	logInfo("starting on", addr)
	server := &http.Server{
		Addr: addr,
	}

	registerRoutes()

	log.Fatal(server.ListenAndServe())
}

func logInfo(args ...any) {
	fmt.Println("[info]", args)
}

func logErr(err error) {
	fmt.Println("[error]", err)
}
