package main

import (
	"net/http"

	"gitlab.com/gitlab-org/cloud-connector-poc/handlers"
)

func registerRoutes() {
	http.Handle("/ai/", handlers.NewAiHandler())
	http.Handle("/", handlers.NewDefaultHandler())
}
