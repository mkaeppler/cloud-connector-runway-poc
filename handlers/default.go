package handlers

import (
	"bytes"
	"net/http"
)

func NewDefaultHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
		w.Write(bytes.NewBufferString("Neither here nor there").Bytes())
	})
}
