package handlers

import (
	"bytes"
	"net/http"
)

func NewAiHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write(bytes.NewBufferString("*bleep* *blop*").Bytes())
	})
}