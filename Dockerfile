FROM golang:1.20-bookworm as base_image
FROM base_image AS install_image

RUN apt-get update && apt-get install -y \
    build-essential

COPY . /app/

WORKDIR /app
RUN make build

FROM base_image AS release_image

COPY --from=install_image /app/bin/server /usr/bin/cc-server

CMD cc-server
